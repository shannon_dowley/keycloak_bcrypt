FROM jboss/keycloak:7.0.0
ADD target/keycloak-bcrypt-7.0.0.jar /opt/jboss/keycloak/standalone/deployments/
RUN curl http://central.maven.org/maven2/org/mindrot/jbcrypt/0.4/jbcrypt-0.4.jar > jbcrypt-0.4.jar 
RUN /opt/jboss/keycloak/bin/jboss-cli.sh --command="module add --name=org.mindrot.jbcrypt --resources=jbcrypt-0.4.jar"

